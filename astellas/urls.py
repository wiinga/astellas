from django.contrib import admin
from django.urls import include, path
from django.conf.urls.static import static
from django.conf import settings
from . import views

admin.autodiscover()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('grappelli/', include('grappelli.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),

    path('', views.Home, name="home"),
    path('video/<int:id>/', views.VideoDetail, name="video"),
    path('obrigado/<int:id>/', views.Thanks, name="thanks"),
    path('conferencia/<str:uuid>/', views.Conference, name="conference"),
    path('politica-de-cookies/', views.CookiePolicy, name="cookie_policy"),
    path('obrigado/', views.RedirectThanks, name="redirect_thanks"),

    path('ajax/signup/', views.SignupRequest, name="signup"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)