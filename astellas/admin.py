from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.auth.models import Group
from django.conf import settings
from django.utils.translation import ugettext as _
from django.urls import reverse
from django.contrib.admin import SimpleListFilter
from django import forms
from django.conf.urls import include, url
import html2text

from django.core.exceptions import ValidationError
from astellas import utils
from astellas.models import *


# --------------------- ADMIN FORMS

class ConfigAdminForm(forms.ModelForm):
    class Meta:
        model = Config
        fields = '__all__'

class SignupAdminForm(forms.ModelForm):
    class Meta:
        model = Signup
        fields = '__all__'

class VideoAdminForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = '__all__'

class SpeakerAdminForm(forms.ModelForm):
    class Meta:
        model = Speaker
        fields = '__all__'

# --------------------- ADMIN

class ConfigAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('grp-collapse grp-open',),
            'fields': (('verbose', 'key'), 'help_text',)
        }),
        ('Value', {
            'classes': ('grp-collapse grp-open',),
            'fields': ('typ', 'value_varchar', 'value_text', 'value_image', 'value_file',),
        }),

    )
    form = ConfigAdminForm

    def get_value(self, model_instance):
        if model_instance.value():
            h = html2text.HTML2Text()

            if model_instance.typ == 'varchar':
                return utils.smart_truncate(h.handle(model_instance.value()), 80)
            elif model_instance.typ == 'text' or model_instance.typ == 'richtext':
                return model_instance.value()
            elif model_instance.typ == 'image':
                return "<a href='%s' target='_blank'><img height='100' src='%s'></a>" % (
                    model_instance.value().url, model_instance.value().url)
            elif model_instance.typ == 'file':
                return "<a href='%s' target='_blank'></a>" % model_instance.value()
            else:
                return "Unknown"
        else:
            return "(No value defined)"

    get_value.short_description = _("Value")
    get_value.allow_tags = True
    list_display = ['key', 'get_value', ]

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser



class SignupAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('grp-collapse grp-open',),
            'fields': ('name', 'email', 'entity',('terms','communication'), ('intention', 'confirmation'),'confirmed','uuid',)
        }),
    )
    form = SignupAdminForm
    list_display = ['name', 'email', 'entity','terms','communication', 'creation_date','intention', 'confirmed', 'confirmation']
    list_display_links = ['name', 'email']
    readonly_fields = ['confirmed','uuid',]

    def has_add_permission(self, request):
        return request.user.is_superuser

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser

class VideoAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('grp-collapse grp-open',),
            'fields': ('title', 'youtube','vimeo', 'description', 'excerpt')
        }),
    )
    form = VideoAdminForm
    list_display = ['title', 'creation_date']
    list_display_links = ['title',]

    def has_add_permission(self, request):
        return request.user.is_superuser

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser

class SpeakerAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('grp-collapse grp-open',),
            'fields': ('name', ('position', 'company'), 'speaker_image', 'description')
        }),
    )
    form = SpeakerAdminForm
    list_display = ['name', 'creation_date']
    list_display_links = ['name',]

    def has_add_permission(self, request):
        return request.user.is_superuser

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser

if 'django.contrib.admin' in settings.INSTALLED_APPS:
    admin.site.register(Config, ConfigAdmin)
    admin.site.register(Signup, SignupAdmin)
    admin.site.register(Video, VideoAdmin)
    admin.site.register(Speaker, SpeakerAdmin)

