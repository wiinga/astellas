from django.http import HttpResponse, Http404, HttpResponseForbidden, HttpResponseNotAllowed, HttpResponseRedirect, HttpResponseBadRequest, HttpResponseNotFound
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic.base import TemplateView, TemplateResponseMixin, View, ContextMixin
from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext as _
from datetime import datetime, timezone
from django.views.decorators.csrf import csrf_protect
from django.urls import reverse
from urllib.parse import urlencode
from django.db.models import Sum
from django.http import JsonResponse
from django.db.models import Q
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.contrib.sites.models import Site
from urllib.error import URLError, HTTPError

from astellas.controllers import *
from astellas.forms import *

import json
import datetime
import urllib.request

class ContextedView(ContextMixin):
    def get_context_data(self, **kwargs):
        context = super(ContextedView, self).get_context_data(**kwargs)
        return context

class GenericTemplateView(ContextedView, TemplateView):
    pass

def Home(request):
    template = 'astellas/home.html'
    menu = MenuBuilder(True)
    video_objects = Video.objects.all()
    videos = []
    for video in video_objects:
        thumb = ""
        if video.youtube != "" and video.youtube is not None:
            thumb = "https://img.youtube.com/vi/"+video.youtube+"/0.jpg"
        elif video.vimeo != "" and video.vimeo is not None:
            try:
                response = urllib.request.urlopen("http://vimeo.com/api/v2/video/"+video.vimeo+".json")
                data = json.loads(response.read())
                if len(data) > 0:
                    thumb = data[0]['thumbnail_medium']
            except HTTPError:
                pass
        single_video = {
            "id": video.pk,
            "thumb":thumb,
            "title":video.title,
            "excerpt":video.excerpt
        }
        videos.append(single_video)

    speaker_objects = Speaker.objects.all()
    speakers = []
    for speaker in speaker_objects:
        single_speaker = {
            "image": speaker.speaker_image.url,
            "name": speaker.name,
            "position":speaker.position,
            "company":speaker.company,
            "description": speaker.description
        }
        speakers.append(single_speaker)

    return render(request, template, {
        'menu':menu,
        'videos':videos,
        'speakers':speakers
    })

def CookiePolicy(request):
    template = 'astellas/cookie_policy.html'
    menu = MenuBuilder()

    return render(request, template, {
        'menu':menu
    })

def Conference(request, uuid):
    template = 'astellas/conference.html'
    menu = MenuBuilder()

    invalid_link = False
    try:
        signup = Signup.objects.get(uuid=uuid)
        if signup.confirmed is False:
            invalid_link = True
    except Signup.DoesNotExist:
        invalid_link = True

    if invalid_link is True:
        template = 'astellas/conference_not_found.html'

    return render(request, template, {
        'menu':menu
    })

def VideoDetail(request, id):
    template = 'common/video.html'

    embed_code = ""
    title = ""
    description = ""

    try:
        video = Video.objects.get(pk=id)
        site = Site.objects.get_current()
        if video.youtube != "" and video.youtube is not None:
            embed_code = '<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'+video.youtube+'?iv_load_policy=3&modestbranding=1&rel=0&showinfo=0&autoplay=1&mute=1&origin='+site.domain +'" allowfullscreen=""></iframe>'
        elif video.vimeo != "" and video.vimeo is not None:
            embed_code = '<iframe src="https://player.vimeo.com/video/'+video.vimeo+'?autoplay=1&loop=1&autopause=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>'
        title = video.title
        description = video.description
    except Video.DoesNotExist:
        pass

    return render(request, template, {
        "embed":embed_code,
        "title":title,
        "description":description
    })

def Thanks(request, id):
    template = 'astellas/thanks.html'
    menu = MenuBuilder()

    try:
        checker = Signup.objects.get(pk=id)
        return render(request, template, {
            'menu': menu
        })
    except Signup.DoesNotExist:
        return redirect(reverse('home'))


def RedirectThanks(request):
    return redirect(reverse('home'))


@csrf_protect
def SignupRequest(request):
    if request.method == 'POST':
        name = request.POST.get("name")
        email_address = request.POST.get("email")
        entity = request.POST.get("entity")
        attend = request.POST.get("attend")
        print(attend)
        print("---")
        terms_get = request.POST.get("terms")
        communication_get = request.POST.get("communication")
        terms = False
        communication = False
        if terms_get == "true":
            terms = True
        if communication_get == "true":
            communication = True

        check_signups = Signup.objects.filter(email=email_address).count()
        if check_signups == 0:
            signup = Signup(name=name, email=email_address, entity=entity, terms=terms, communication=communication, intention=attend)
            signup.save()

            outcome = {
                'response': 'success',
                'id':signup.pk
            }
        else:
            outcome = {
                'response': 'exists'
            }
    else:
        outcome = {'response':'invalid'}
    return JsonResponse(outcome, safe=False)