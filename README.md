# ASTELLAS #

### pip requirements ###

asgiref==3.4.1  
Django==3.2.7  
django-bootstrap-form==3.4  
django-ckeditor==6.1.0  
django-ckeditor-updated==4.4.4  
django-cleanup==5.2.0  
django-grappelli==2.15.1  
django-js-asset==1.2.2  
django-pipeline==2.0.6  
html2text==2020.1.16  
Pillow==8.3.2  
psycopg2==2.9.1  
pytz==2021.1  
sqlparse==0.4.1  
typing-extensions==3.10.0.2  
  
  
psycopg2 installation may require the following:  
apt-get install python3-dev libpq-dev