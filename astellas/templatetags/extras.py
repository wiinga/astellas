from django import template

# RTFM: https://docs.djangoproject.com/en/dev/howto/custom-template-tags/

register = template.Library()

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)
