from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext as _
from django.conf import settings
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.sites.shortcuts import get_current_site
from ckeditor.fields import RichTextField
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.contrib.sites.models import Site

import random
import os
import urllib
import datetime
import uuid

def file_upload_to(instance, filename):
    return ''.join(['configs/', str(uuid.uuid4()), '.' + filename.split('.')[-1]])

def speaker_upload_to(instance, filename):
    return ''.join(['speakers/', str(uuid.uuid4()), '.' + filename.split('.')[-1]])


class Config(models.Model):
    CONFIG_TYPES = (('varchar', 'Simple Value'),
                    ('text', 'Text'),
                    ('image', 'Image'),
                    ('file', 'File'),
                    )
    key = models.CharField(_('Variable'), max_length=255, blank=False, null=False, unique=True, db_index=True)
    verbose = models.CharField(_('Name'), max_length=255, blank=True, null=True, default=None, unique=True)
    help_text = models.CharField(_('Description'), max_length=255, blank=True, null=True, default='')
    typ = models.CharField('Type', max_length=20, blank=False, null=False, choices=CONFIG_TYPES, default='varchar')
    value_varchar = models.CharField('Value', max_length=255, blank=True, null=True, default=None)
    value_text = models.TextField('Text', blank=True, null=True, default=None)
    value_file = models.FileField('File', upload_to=file_upload_to, blank=True, null=True, default=None)
    value_image = models.ImageField(max_length=255, upload_to=file_upload_to, blank=True, null=True,
                                       default=None)

    def value(self):
        return getattr(self, 'value_%s' % self.typ)

    class Meta:
        ordering = ['id', 'key', 'verbose']
        verbose_name = _('Configuration')
        verbose_name_plural = _('Configurations')
        permissions = (
            ('can_modify_keys', 'Can modify Config keys, verbose names and descriptions'),
        )

    def __str__(self):
        return self.verbose if self.verbose else self.key

class Signup(models.Model):
    ATTEND_INTENT_CHOICES = ( ('p', _('Presencial')),
                              ('v', _('Virtual'))
                            )

    ATTEND_CONFIRMATION_CHOICES = ( ('n', _('Não Confirmado')),
                                    ('p', _('Confirmado Presencial')),
                                    ('v', _('Confirmado Virtual'))
                                )

    name = models.CharField(_('Nome'), max_length=300, blank=False, null=False, unique=False)
    email = models.CharField(_('Email'), max_length=300, blank=False, null=False, unique=False)
    entity = models.CharField(_('Entidade'), max_length=300, blank=True, null=True, unique=False)
    uuid = models.CharField(_('UUID'), max_length=300, blank=False, null=False, unique=False, default="")
    terms = models.BooleanField(_('Aceita os Termos e Privacidade'), blank=False, null=False, default=True)
    communication = models.BooleanField(_('Aceita Comunicação da Astellas'), blank=False, null=False, default=False)
    intention = models.CharField(_('Inscrição'), max_length=20, blank=False, null=False, choices=ATTEND_INTENT_CHOICES, default='v')
    confirmation = models.CharField(_('Confirmação'), max_length=20, blank=False, null=False,
                                 choices=ATTEND_CONFIRMATION_CHOICES, default='n')
    confirmed = models.BooleanField(_('Inscrição Confirmada'), blank=False, null=False, default=False)
    creation_date = models.DateTimeField(_('Data Criação'), null=False, blank=False, auto_now_add=True)

    class Meta:
        verbose_name = _('Inscrição')
        verbose_name_plural = _('Inscrições')

    def save(self, *args, **kwargs):
        confirmation_mail = 'n'
        if self.pk:
            oldSignup = Signup.objects.get(pk=self.pk)
            if not oldSignup.confirmed and self.confirmation != 'n':
                self.confirmed = True
                if self.intention == self.confirmation:
                    if self.confirmation == 'p':
                        confirmation_mail = 'physical'
                    else:
                        confirmation_mail = 'virtual'
                else:
                    confirmation_mail = 'changed'
        else:
            self.uuid = str(uuid.uuid4())
        super(Signup, self).save(*args, **kwargs)

        # ----------------------------------------------------------------
        if confirmation_mail != 'n':
            config_objects = Config.objects.all()
            configs = dict(zip(map(lambda x: getattr(x, 'key'), config_objects), config_objects))
            subject = "CONFERÊNCIA ASTELLAS MEDICINA HUMANIZADA | CONFIRMAÇÃO DE INSCRIÇÃO"
            if confirmation_mail == 'changed':
                subject = "CONFERÊNCIA ASTELLAS MEDICINA HUMANIZADA | CONFIRMAÇÃO PRESENÇA EVENTO LIVE"
            from_email = configs['EMAIL_FROM'].value_varchar
            reply_to = [configs['EMAIL_REPLY_TO'].value_varchar]
            site_url = Site.objects.get_current().domain
            body = render_to_string('email/confirmation_'+confirmation_mail+'.html')
            body = body.replace("#link#",site_url+"/conferencia/"+self.uuid)
            email = EmailMessage(
                subject=subject,
                body=body,
                from_email=from_email,
                to=[self.email],
                reply_to=reply_to
            )
            email.content_subtype = "html"
            email.send(fail_silently=True)
        # ----------------------------------------------------------------

    def __str__(self):
        return _('Inscrição #%d de %s') % (self.pk, self.name)

class Video(models.Model):

    title = models.CharField(_('Titulo'), max_length=100, blank=False, null=False, unique=False)
    excerpt = models.TextField(_('Excerto'), blank=True, null=True, unique=False)
    youtube = models.CharField(_('Youtube ID'), help_text=_("Insira apenas o ID do video. Ex.: https://www.youtube.com/watch?v=<b><i>pD1bfy84jRg</i></b>."), max_length=300, blank=True, null=True, unique=False)
    vimeo = models.CharField(_('Vimeo ID'), help_text=_("Insira apenas o ID do video. Ex.: https://vimeo.com/<b><i>49987021</i></b>."), max_length=300, blank=True, null=True, unique=False)
    description = RichTextField(_('Descrição'), blank=True, null=True, unique=False)
    creation_date = models.DateTimeField(_('Data Criação'), null=False, blank=False, auto_now_add=True)

    class Meta:
        verbose_name = _('Video')
        verbose_name_plural = _('Videos')

    def save(self, *args, **kwargs):
        super(Video, self).save(*args, **kwargs)

    def __str__(self):
        return '%s' % self.title

class Speaker(models.Model):

    name = models.CharField(_('Nome'), max_length=100, blank=False, null=False, unique=False)
    position = models.CharField(_('Cargo'), max_length=100, blank=False, null=False, unique=False)
    company = models.CharField(_('Empresa'), max_length=100, blank=True, null=True, unique=False)
    speaker_image = models.ImageField(_('Imagem'), help_text="Original: 800x800", max_length=255, upload_to=speaker_upload_to, blank=False, null=False,
                                    default=None)
    description = models.TextField(_('Descrição'), blank=True, null=True, unique=False)
    creation_date = models.DateTimeField(_('Data Criação'), null=False, blank=False, auto_now_add=True)

    class Meta:
        verbose_name = _('Speaker')
        verbose_name_plural = _('Speakers')

    def save(self, *args, **kwargs):
        super(Speaker, self).save(*args, **kwargs)

    def __str__(self):
        return '%s' % self.name
