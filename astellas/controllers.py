from django.urls import reverse

def MenuBuilder(home=False):
    menu = [
        {
            'text':'Home',
            'link': (reverse('home') if not home else '#main-carousel'),
            'isActive': False if not home else True
        },
        {
            'text': 'Sobre',
            'link': (reverse('home') if not home else '') + '#section-event-infos',
            'isActive': False
        },
        {
            'text': 'Inscrições',
            'link': (reverse('home') if not home else '') + '#section-register',
            'isActive': False
        },
        {
            'text': 'Evento Seguro',
            'link': (reverse('home') if not home else '') + '#section-seguro-content',
            'isActive': False
        },
        {
            'text': 'Agenda',
            'link': (reverse('home') if not home else '') + '#section-schedule',
            'isActive': False
        },
        {
            'text': 'Speakers',
            'link': (reverse('home') if not home else '') + '#section-speakers',
            'isActive': False
        },
        {
            'text': 'Edições Anteriores',
            'link': (reverse('home') if not home else '') + '#section-blog',
            'isActive': False
        }
    ]

    return menu