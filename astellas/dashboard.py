"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = '_dev.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import gettext_lazy as _
from django.urls import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(modules.ModelList(
            _('Sistema'),
            column=1,
            collapsible=True,
            css_classes=('grp-closed',),
            models=('django.contrib.auth.models.Group', 'django.contrib.sites.*', 'astellas.models.Config',),
        ))

        self.children.append(modules.ModelList(
            _('Astellas'),
            column=1,
            collapsible=False,
            models=('django.contrib.auth.models.User','astellas.models.Signup','astellas.models.Video','astellas.models.Speaker'),
        ))

        self.children.append(modules.LinkList(
            _('Suporte'),
            column=2,
            children=[
                {
                    'title': _('Equipa de Desenvolvimento'),
                    'url': 'mailto:info@wiinga.pt',
                    'external': True,
                },
            ]
        ))

