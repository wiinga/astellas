(function($) {
    "use strict";
    $(function () {

        $(document).ready(function(){


            //Intro - Register Form Validator and Ajax Sender

            $("#register-form").validate({
                lang : 'pt_PT',
                submitHandler: function(form) {
                    $('.form-error').hide();
                    var terms = $('#terms').prop('checked');
                    var communication = $('#communications').prop('checked');
                    $('#termslabel').removeClass('withError');
                    if(!terms){
                        $('#termslabel').addClass('withError');
                    }
                    else{
                        if(!$('.btn-form').hasClass('loading')){
                            $('.btn-form').addClass('loading');
                            $.ajax({
                                type: "POST",
                                url: "/ajax/signup/",
                                headers: {
                                    'X-CSRFToken': $('input[name=csrfmiddlewaretoken]').val()
                               },
                                data: {
                                  "name": $("#register-form #name").val(),
                                  "email": $("#register-form #email").val(),
                                  "entity": $("#register-form #entity").val(),
                                  "attend": $("#register-form #attend").val(),
                                  "terms": terms,
                                  "communication":communication
                                },
                                dataType: "json",
                                success: function (data) {
                                    if (data.response == "success") {
                                          window.location.href = "/obrigado/"+data.id;
                                    } else if (data.response == "exists") {
                                        $('.btn-form').removeClass('loading');
                                        $('#form-warning').show();
                                    } else {
                                        $('.btn-form').removeClass('loading');
                                        $('#form-error').show();
                                    }
                                },
                                error: function(){
                                    $('.btn-form').removeClass('loading');
                                    $('#form-error').show();
                                }

                            });
                        }
                    }
                }
            });

        });

    });
})(jQuery)
