from astellas.models import *
from django.db.models import Sum


def configurations(request):
    context = {}

    if Config.objects.count() == 0:
        object = Config(verbose="Site Name",
                           key = "SITE_NAME",
                           help_text = "Site Name",
                           value_varchar = "Conferência Astellas - Medicina Regenerativa")
        object.save()

        object = Config(verbose="Site Cookie URL",
                           key="SITE_COOKIE_URL",
                           help_text="Site Cookie URL",
                           value_varchar="localhost")
        object.save()

        object = Config(verbose="Cookie Banner Text",
                        key="COOKIE_BANNER_TEXT",
                        help_text="Cookie Banner Text",
                        value_varchar="Usamos cookies no nosso site para melhorar o desempenho e experiência. Ao continuar, declara aceitar todos os cookies.")
        object.save()

        object = Config(verbose="Email Subject",
                        key="EMAIL_SUBJECT",
                        help_text="Confirmation Email Subject",
                        value_varchar="CONFERÊNCIA ASTELLAS MEDICINA HUMANIZADA | CONFIRMAÇÃO DE PEDIDO DE INSCRIÇÃO")
        object.save()

        object = Config(verbose="Email From",
                        key="EMAIL_FROM",
                        help_text="Confirmation Email From",
                        value_varchar="Conferência Astellas <conferencia@astellasconferencia.pt>")
        object.save()

        object = Config(verbose="Email Reply To",
                        key="EMAIL_REPLY_TO",
                        help_text="Confirmation Email Reply To",
                        value_varchar="portugal@astellas.com")


        object.save()

        object = Config(verbose="Terms Page",
                        key="TERMS_CONDITIONS",
                        help_text="Terms And Conditions Page Link",
                        value_varchar="https://www.astellas.com/pt/terms-of-use")
        object.save()

        object = Config(verbose="Privacy Policy",
                        key="PRIVACY_POLICY",
                        help_text="Privacy Policy Page Link",
                        value_varchar="https://www.astellas.com/pt/privacy-policy")
        object.save()

        object = Config(verbose="Zoom Conference",
                        key="ZOOM_CONFERENCE_LINK",
                        help_text="Zoom Conference Link",
                        value_varchar="https://www.astellas.com/")
        object.save()

        object = Config(verbose="Zoom Conference Text",
                        key="ZOOM_CONFERENCE_COPY",
                        help_text="Zoom Conference Copy Text",
                        value_varchar="No dia 21 de Outubro ficará disponível nesta página o link para que possa assistir à conferência via Zoom.")
        object.save()

    configs = Config.objects.all()
    context['configs'] = dict(zip(map(lambda x: getattr(x,'key'),configs),configs))
    return context
